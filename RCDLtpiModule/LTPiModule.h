#ifndef LTPIMODULE_H
#define LTPIMODULE_H

#include <string>
#include <vector>
#include <iostream>

#include "RCDLtpi/LTPI.h"
#include "ROSCore/ReadoutModule.h"
#include "DFSubSystemItem/Config.h"

#include "ipc/partition.h"

#include "LTPidal/LTPiModule.h"
#include "LTPidal/LTPiConfigBase.h"
#include "LTPidal/LTPi_Bypass.h"
#include "LTPidal/LTPi_CTPSlave.h"
#include "LTPidal/LTPi_Expert.h"
#include "LTPidal/LTPi_LTPMaster.h"
#include "LTPidal/LTPi_LTPSlave.h"

#include "is/info.h"

namespace RCD {

  using namespace ROS;
  using namespace RCD;

  class LTPiModule : public ReadoutModule
  {
   public:

    /** The constructor should have no arguments */
    LTPiModule ();

    virtual ~LTPiModule () noexcept;
    virtual const std::vector<DataChannel *> *channels ();
    virtual void setup (DFCountedPointer<Config> configuration);
    virtual void clearInfo () ;
    virtual DFCountedPointer<Config> getInfo () ;
    virtual void configure(const daq::rc::TransitionCmd&);

//     virtual void connect() ;
//     virtual void prepareForRun();

    virtual void publish();
    virtual void publishFullStats();
    
//     virtual void disconnect();
    virtual void unconfigure(const daq::rc::TransitionCmd&);

//     virtual void userCommand(std::string &commandName,
// 			     std::string &argument);

   private:
    virtual void configureBypass(const LTPidal::LTPi_Bypass* c);
    virtual void configureCTPSlave(const LTPidal::LTPi_CTPSlave* c);
    virtual void configureExpert(const LTPidal::LTPi_Expert* c);
    virtual void configureLTPMaster(const LTPidal::LTPi_LTPMaster* c);
    virtual void configureLTPSlave(const LTPidal::LTPi_LTPSlave* c);

    // Status (0 if a given method was successful)
    int m_status;

    LTPI* m_ltpi;
    VME*  m_vme;

    // for IS
    IPCPartition m_ipcpartition;
    daq::core::Partition* m_partition;
    Configuration* m_confDB;
    LTPidal::LTPiModule* m_dbModule;
    DFCountedPointer<Config> m_configuration;

    // configuration parameters
    std::string m_UID;
    unsigned int m_PhysAddress;
    std::string m_OperationMode;
    std::string m_IS_server;
    LTPidal::LTPiConfigBase* m_LTPiConfig;

    bool m_useConfiguration;
    bool m_useMonitoring;

    // busy
    std::string m_busy_ctplink_uid;
    std::string m_busy_ltplink_uid;
    bool m_busy_with_ctplink;
    bool m_busy_with_ltplink;

    int m_CTP_in_gain;
    int m_LTP_in_gain;
    int m_CTP_in_equ;
    int m_LTP_in_equ;
    int m_Delay_L1A;
    int m_Delay_Orbit;
    int m_Delay_TTR1;
    int m_Delay_TTR2;
    int m_Delay_TTR3;
    int m_Delay_TTYPE0;
    int m_Delay_TTYPE1;
    int m_Delay_TTYPE2;
    int m_Delay_TTYPE3;
    int m_Delay_TTYPE4;
    int m_Delay_TTYPE5;
    int m_Delay_TTYPE6;
    int m_Delay_TTYPE7;
    int m_Delay_BGO0;
    int m_Delay_BGO1;
    int m_Delay_BGO2;
    int m_Delay_BGO3;

  };
  inline const std::vector<DataChannel *> *LTPiModule::channels ()
  {
    return 0;
  }
}

#endif //LTPIMODULE_H
