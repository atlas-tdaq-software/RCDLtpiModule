#include <iostream>
#include <sstream>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <list>

#include "config/Configuration.h"

#include "RODBusydal/BusyChannel.h"
#include "config/Configuration.h"
#include "dal/Resource.h"
#include "dal/Detector.h"
#include "dal/Partition.h"
#include "dal/util.h"

#include "RCDLtpiModule/LTPiModule.h"
#include "RCDLtpi/RCDLtpi.h"

#include "ers/ers.h"
#include "TTCInfo/LTPi_ISNamed.h"

#include "ipc/partition.h"
#include "is/info.h"


using namespace RCD;
using namespace ROS;

/******************************************************/
LTPiModule::LTPiModule() :
  m_ltpi(nullptr),
  m_ipcpartition(getenv("TDAQ_PARTITION")),
  m_partition(nullptr),
  m_confDB(nullptr),
  m_dbModule(nullptr),
  m_configuration(),
  m_UID(""),
  m_PhysAddress(0x00),
  m_OperationMode(""),
  m_IS_server(""),
  m_LTPiConfig(nullptr),
  m_useConfiguration(false),
  m_useMonitoring(false),
  m_busy_ctplink_uid(""),
  m_busy_ltplink_uid(""),
  m_busy_with_ctplink(false),
  m_busy_with_ltplink(false), 
  m_CTP_in_gain(-1),
  m_LTP_in_gain(-1),
  m_CTP_in_equ(-1),
  m_LTP_in_equ(-1),
  m_Delay_L1A(-1),
  m_Delay_Orbit(-1),
  m_Delay_TTR1(-1),
  m_Delay_TTR2(-1),
  m_Delay_TTR3(-1),
  m_Delay_TTYPE0(-1),
  m_Delay_TTYPE1(-1),
  m_Delay_TTYPE2(-1),
  m_Delay_TTYPE3(-1),
  m_Delay_TTYPE4(-1),
  m_Delay_TTYPE5(-1),
  m_Delay_TTYPE6(-1),
  m_Delay_TTYPE7(-1),
  m_Delay_BGO0(-1),
  m_Delay_BGO1(-1),
  m_Delay_BGO2(-1),
  m_Delay_BGO3(-1)
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::LTPiModule() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "Done");
}

/******************************************************/
LTPiModule::~LTPiModule() noexcept
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::~LTPiModule() ";
  ERS_LOG(sout << "Entered");

 if (m_ltpi!=nullptr) {
    delete m_ltpi;
    m_ltpi=nullptr;
  }

  ERS_LOG(sout << "Done");
}

/******************************************************************/
void LTPiModule::setup(DFCountedPointer<Config> configuration)
/******************************************************************/
{
  std::string sout = m_UID + "@LTPiModule::setup() ";
  ERS_LOG(sout << "Entered");

  m_UID = configuration->getString("UID");
  if (m_UID == "") {
    ERS_LOG(sout << "Name parameter is \"\", setting to default: \"LTPiModule_without_name\"");
    m_UID = "LTPiModule_without_name";
  } 
  sout = m_UID + "@LTPiModule::setup() ";

  try {
    m_confDB = configuration->getPointer<Configuration>("configurationDB");
  } catch (CORBA::SystemException& ex) {
    ostringstream text;
    text << sout << "Creating configrationDB error : CORBA::SystemException ex._name()= " << ex._name();
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );

  } catch (std::exception& ex) {
    ostringstream text;
    text << sout << "Creating configrationDB error : std::exception ex.what()=" << ex.what();
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "UID = " << m_UID);

  m_PhysAddress = configuration->getUInt("PhysAddress");
  ERS_LOG(sout << "VMEbus address = 0x" << hex << m_PhysAddress << dec);

  m_OperationMode = configuration->getString("OperationMode");
  ERS_LOG(sout << "OperationMode = " << m_OperationMode);

  if (m_OperationMode==LTPidal::LTPiModule::OperationMode::Configure_only) {
    m_useConfiguration = true;
    m_useMonitoring = false;
  } else if (m_OperationMode==LTPidal::LTPiModule::OperationMode::Monitoring_only) {
    m_useConfiguration = false;
    m_useMonitoring = true;

  } else if (m_OperationMode==LTPidal::LTPiModule::OperationMode::Configure_and_monitoring) {
    m_useConfiguration = true;
    m_useMonitoring = true;
  } else {
      ostringstream text;
      text << sout << "Undefined value \"" << m_OperationMode << "\" for attribute OperationMode";
      ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "m_useConfiguration = " << m_useConfiguration);
  ERS_LOG(sout << "m_useMonitoring = " << m_useMonitoring);

  m_IS_server = configuration->getString("IS_server");    
  ERS_LOG(sout << "m_IS_server = " << m_IS_server);
  // get configuration object and store it in a member variable
  if (m_useConfiguration) {
    ERS_LOG(sout << "Configuring");

    m_LTPiConfig = nullptr;

    // get partition object
    m_partition= const_cast<daq::core::Partition*>(daq::core::get_partition(*m_confDB, m_ipcpartition.name()));
    m_dbModule = const_cast<LTPidal::LTPiModule*>(m_confDB->get<LTPidal::LTPiModule>(m_UID));
    m_LTPiConfig = const_cast<LTPidal::LTPiConfigBase*>(m_dbModule->get_Configuration());

    // get busy flags from the database, some of them might not be relevant for certain configurations

    // first check whether the module itself is enabled
    if (!m_dbModule->daq::core::ResourceSet::disabled(*m_partition)) {
      ERS_LOG(sout << "LTPModule \"" << m_dbModule->UID() << "\" itself is enabled. Configuring busy mask.");
      // get the busy channels linked to "Contains"
      std::vector<const daq::core::ResourceBase*> items = m_dbModule->get_Contains();
      std::vector<const daq::core::ResourceBase*>::iterator it = items.begin();
      for(;it != items.end();it++){
	//check that it is a BusyChannel
	const RODBusydal::BusyChannel* bc = m_confDB->cast<RODBusydal::BusyChannel> (*it);
	if(bc){
	  int channelnumber = bc->get_Id();
	  ERS_LOG(sout << "LTPiModule::setup: found BusyChannel \"" << bc->UID() << "\" for channel=" << channelnumber);
	  
	  bool resource_enabled(false);
	  
	  // first check whether the busy channel is enabled
	  if (!bc->disabled(*m_partition)) {
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" is enabled. Checking corresponding BusySource.");
	    
	    const daq::core::ResourceBase* rb = bc->get_BusySource();
	    if (rb) {
	      if (!rb->disabled(*m_partition)) {
		// resource is enabled
		ERS_LOG(sout << "LTPiModule::setup: Resource for " << bc->UID() << "@BusyChannel (Id=" << channelnumber << ") is enabled -> enabling channel");
		resource_enabled = true;
	      } else {
		ERS_LOG(sout << "Resource for BusyChannel \"" << bc->UID() << "\" (" << channelnumber << ") is disabled -> disabling channel.");
		resource_enabled = false;
	      }
	    }
	  } else {
	    ERS_LOG(sout << "BusyChannel \"" << bc->UID() << "\" is disabled. Disabling this channel in the LTP.");
	  }
      
	  if (channelnumber == 0) {
	    m_busy_with_ctplink = resource_enabled;
	    m_busy_ctplink_uid = bc->UID();
	  } else if (channelnumber == 1) {
	    m_busy_with_ltplink = resource_enabled;
	    m_busy_ltplink_uid = bc->UID();
	  } else {
	    ostringstream text;
	    text << sout << "Invalid Id (" << channelnumber << ") for " << bc->UID() << "@BusyChannel.  Must be 0 (CTP link) or 1 (LTP link).";
	    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
	  }
	}
      }
    }
    ERS_LOG(sout << "BusyChannel(0): \"" << m_busy_ctplink_uid << "\" - m_busy_with_ctplink = " << m_busy_with_ctplink);
    ERS_LOG(sout << "BusyChannel(1): \"" << m_busy_ltplink_uid << "\" - m_busy_with_ltplink = " << m_busy_with_ltplink);
  }
  
  m_IS_server = configuration->getString("IS_server");    
  ERS_LOG(sout << "m_IS_server = " << m_IS_server);

  // configure hardware
  // do it in setup to make sure the clock is delivered before configure is reached

  m_status = 0;
  
  // Open LTPi
 
  m_vme = VME::Open();
  m_ltpi = new LTPI( m_vme, m_PhysAddress );
  if( ( m_status = (*m_ltpi)() ) != LTPI::SUCCESS ) {
    ostringstream text;
    text << sout << "Could not open VME at PhysAddress 0x" << hex << m_PhysAddress << dec;
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  } else {
    ERS_LOG(sout << "LTPI created at PhysAddress 0x" << hex << m_PhysAddress << dec);
  }
  
  bool ready;
  if((m_status = m_ltpi->ready(ready)) != LTPI::SUCCESS) {
    ostringstream text;
    text << sout << "Could not read ready status of LTPI";
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  } 
  else {
    ERS_LOG(sout << "LTPI ready");
  }
 
  if(!ready) {
    if( ( m_status = m_ltpi->init()) != LTPI::SUCCESS ) {
      ostringstream text;
      text << sout << "Could not initialize LTPI";
      ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
    } else {
      ERS_LOG(sout << "LTPI initialized");
    }
    }

 if (m_useConfiguration) {

    if (const LTPidal::LTPi_Bypass* ltpiConf = m_confDB->cast<LTPidal::LTPi_Bypass>(m_LTPiConfig)) {
      ERS_LOG(sout << "Found LTPi_Bypass configuration");
      this->configureBypass(ltpiConf);
    } 
    else if (const LTPidal::LTPi_CTPSlave* ltpiConf = m_confDB->cast<LTPidal::LTPi_CTPSlave>(m_LTPiConfig)) {
      ERS_LOG(sout << "Found LTPi_CTPSlave configuration");
      this->configureCTPSlave(ltpiConf);
    } 
    else if (const LTPidal::LTPi_Expert* ltpiConf = m_confDB->cast<LTPidal::LTPi_Expert>(m_LTPiConfig)) {
      ERS_LOG(sout << "Found LTPi_Expert configuration");
      this->configureExpert(ltpiConf);
    } 
    else if (const LTPidal::LTPi_LTPMaster* ltpiConf = m_confDB->cast<LTPidal::LTPi_LTPMaster>(m_LTPiConfig)) {
      ERS_LOG(sout << "Found LTPi_LTPMaster configuration");
      this->configureLTPMaster(ltpiConf);
    } 
    else if (const LTPidal::LTPi_LTPSlave* ltpiConf = m_confDB->cast<LTPidal::LTPi_LTPSlave>(m_LTPiConfig)) {
      ERS_LOG(sout << "Found LTPi_LTPSlave configuration");
      this->configureLTPSlave(ltpiConf);
    } 
    else {   // else: no configuration
      ostringstream text;
      text << sout << "No valid LTPiConfigBase provided as Configuration";
      ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
    } 
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
void LTPiModule::configure(const daq::rc::TransitionCmd&)
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::configure() ";
  ERS_LOG(sout << "Entered");

  ERS_LOG(sout << "LTPiModule::configure: Done");
}

/**********************************************************/
void LTPiModule::unconfigure(const daq::rc::TransitionCmd&)
/**********************************************************/
{
  std::string sout = m_UID + "@LTPiModule::unconfigure() ";
  ERS_LOG(sout << "Entered");

  if (m_ltpi!=nullptr) {
    delete m_ltpi;
    m_ltpi=nullptr;
  }

  ERS_LOG(sout << "Done");
}

/******************************************************/
DFCountedPointer<Config> LTPiModule::getInfo()
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::getInfo() ";
  ERS_DEBUG(1, sout << "Entered");

  DFCountedPointer<Config> configuration = Config::New();
  
  ERS_DEBUG(1, sout << "Done");
  return configuration;
}

/******************************************************/
void LTPiModule::clearInfo()
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::clearInfo() ";
  ERS_DEBUG(1, sout << "Entered");
  ERS_DEBUG(1, sout << "Done");
}

/******************************************************/
void LTPiModule::publish()
/******************************************************/
{
  std::string sout = m_UID + "@LTPiModule::publish() ";
  ERS_DEBUG(1, sout << "Entered");

  if (m_useMonitoring) {
    const std::string dict_entry = m_IS_server + "." + m_UID + "/Ltpi";
    ERS_DEBUG(1, sout << dict_entry.c_str());
    
    LTPi_ISNamed is_entry(m_ipcpartition, dict_entry);
    int status = 0;
    u_short val = 0;

    // cannot read back value due to arbitration problems due to VME traffic
    // --> use configuration parameters for the delays

    is_entry.CTP_gain = m_CTP_in_gain;
    is_entry.CTP_equ = m_CTP_in_equ;
    is_entry.LTP_gain = m_LTP_in_gain;
    is_entry.LTP_equ = m_LTP_in_equ;
    is_entry.Delay_L1A = m_Delay_L1A;
    is_entry.Delay_Orbit = m_Delay_Orbit;
    is_entry.Delay_TTR1 = m_Delay_TTR1;
    is_entry.Delay_TTR2 = m_Delay_TTR2;
    is_entry.Delay_TTR3 = m_Delay_TTR3;
    is_entry.Delay_TTYPE0 = m_Delay_TTYPE0;
    is_entry.Delay_TTYPE1 = m_Delay_TTYPE1;
    is_entry.Delay_TTYPE2 = m_Delay_TTYPE2;
    is_entry.Delay_TTYPE3 = m_Delay_TTYPE3;
    is_entry.Delay_TTYPE4 = m_Delay_TTYPE4;
    is_entry.Delay_TTYPE5 = m_Delay_TTYPE5;
    is_entry.Delay_TTYPE6 = m_Delay_TTYPE6;
    is_entry.Delay_TTYPE7 = m_Delay_TTYPE7;
    is_entry.Delay_BGo0 = m_Delay_BGO0;
    is_entry.Delay_BGo1 = m_Delay_BGO1;
    is_entry.Delay_BGo2 = m_Delay_BGO2;
    is_entry.Delay_BGo3 = m_Delay_BGO3;
				 
    LTPI::TTYP_SELECTION typ;
    LTPI::LOCTTYP_SELECTION loc;
    status = m_ltpi->readTriggerTypeSelection(typ,loc);
    is_entry.TTYPE_MODE = LTPI::LOCTTYP_SELECTION_NAME[loc];

    status = m_ltpi->readLocalTriggerTypeWord(0,val);
    is_entry.TTYPE_REGISTER = val;
    is_entry.LUT_TTYPE_000 = val;
    status = m_ltpi->readLocalTriggerTypeWord(1,val);
    is_entry.LUT_TTYPE_001 = val;
    status = m_ltpi->readLocalTriggerTypeWord(2,val);
    is_entry.LUT_TTYPE_010 = val;
    status = m_ltpi->readLocalTriggerTypeWord(3,val);
    is_entry.LUT_TTYPE_011 = val;
    status = m_ltpi->readLocalTriggerTypeWord(4,val);
    is_entry.LUT_TTYPE_100 = val;
    status = m_ltpi->readLocalTriggerTypeWord(5,val);
    is_entry.LUT_TTYPE_101 = val;
    status = m_ltpi->readLocalTriggerTypeWord(6,val);
    is_entry.LUT_TTYPE_110 = val;
    status = m_ltpi->readLocalTriggerTypeWord(7,val);
    is_entry.LUT_TTYPE_111 = val;
    
   // Check status registers
    bool isBusy;
    LTPI::BUSY_SELECTION busy_status;
    status = m_ltpi->readSignalStatus(LTPI::STATUS_BUSY,LTPI::CTP,isBusy);
    is_entry.Busy_CTPout = ((isBusy == 1) ? 1 : 0);
    status = m_ltpi->readSignalStatus(LTPI::STATUS_BUSY,LTPI::LTP,isBusy);
    is_entry.Busy_LTPout = ((isBusy == 1) ? 1 : 0);
    status = m_ltpi->readBusySelection(LTPI::CTP,busy_status); 
    is_entry.Busy_CTP = LTPI::BUSY_SELECTION_DESC[busy_status];
    status = m_ltpi->readBusySelection(LTPI::LTP,busy_status); 
    is_entry.Busy_LTP = LTPI::BUSY_SELECTION_DESC[busy_status];
    status = m_ltpi->readBusySelection(LTPI::NIM,busy_status); 
    is_entry.Busy_NIM = LTPI::BUSY_SELECTION_DESC[busy_status];
    status = m_ltpi->readSignalStatus(LTPI::STATUS_BUSY,LTPI::CTP,isBusy);
    is_entry.Busy_CTP_out = ((isBusy == 1) ? 1 : 0);
    status = m_ltpi->readSignalStatus(LTPI::STATUS_BUSY,LTPI::LTP,isBusy);
    is_entry.Busy_LTP_out = ((isBusy == 1) ? 1 : 0);

   
    LTPI::SIGNAL_SELECTION signal_path;
    status = m_ltpi->readSignalSelection(LTPI::SELECT_BC,signal_path);
    is_entry.BC_routing  = LTPI::SIGNAL_SELECTION_DESC[signal_path];
    status = m_ltpi->readSignalSelection(LTPI::SELECT_ORB,signal_path);
    is_entry.ORB_routing  = LTPI::SIGNAL_SELECTION_DESC[signal_path];
    status = m_ltpi->readSignalSelection(LTPI::SELECT_L1A,signal_path);
    is_entry.L1A_routing  = LTPI::SIGNAL_SELECTION_DESC[signal_path];
    status = m_ltpi->readSignalSelection(LTPI::SELECT_TTRG,signal_path);
    is_entry.TTR_routing  = LTPI::SIGNAL_SELECTION_DESC[signal_path];
    status = m_ltpi->readSignalSelection(LTPI::SELECT_BGO,signal_path);
    is_entry.BGO_routing  = LTPI::SIGNAL_SELECTION_DESC[signal_path];
    status = m_ltpi->readTriggerTypeSelection(typ,loc);
    is_entry.TRT_routing  = LTPI::TTYP_SELECTION_DESC[typ];
    LTPI::CALREQ_SELECTION calreq;
    status = m_ltpi->readCalibrationRequestSelection(calreq);
    is_entry.CAL_routing  = LTPI::CALREQ_SELECTION_DESC[calreq];;
 

    try {
      is_entry.checkin();
    } catch (CORBA::SystemException& ex) {
      ostringstream text;
      text << sout << "Could not publish to IS: CORBA::SystemException ex._name()=" << ex._name();
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    } catch (daq::is::Exception & ex) {
      ostringstream text;
      text << sout << "Could not publish to IS: daq::is::Exception ex.what()=" << ex.what();
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    } catch (std::exception& ex) {
      ostringstream text;
      text << sout << "Could not publish to IS: std::exception ex.what()=" << ex.what();
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
    } catch (...) {
      ostringstream text;
      text << sout << "Could not publish to IS. Unknown exception. Rethrowing.";
      ERS_REPORT_IMPL( ers::warning, ers::Message, text.str(), );
      throw;
    }
  }
  ERS_DEBUG(1, sout << "Done");
}
 
/******************************************************/
void LTPiModule::publishFullStats()
/******************************************************/
{  
  std::string sout = m_UID + "@LTPiModule::publishFullStats() ";
  ERS_DEBUG(1, sout << "Entered");

  // publish counter value
  this->publish();

  ERS_DEBUG(1, sout << "Done");
}    


void LTPiModule::configureBypass(const LTPidal::LTPi_Bypass* c) {
  std::string sout = m_UID + "@LTPiModule::configureBypass() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "called with LTPi_Bypass = " << c);
  c->print(1, true, std::cout);
  u_int a_LTPcable_length = c->get_LTPcable_length();
  ERS_LOG(sout << "LTPcable_length = " << a_LTPcable_length);
  
 // configure
  // in this mode the ltpi is supposed to be already initialized and running. 
  // Consequently the I2C is not set/reset. The i2c clock frequency is checked,
  // if it is not properly set i2c is reset.
  u_short val_gain, val_ctrl;
  double frequency;
  int status = 0;

  // configure busy mask according to values obtained from database during setup()
  // touching only the ltp link in. Either busy from ltp link out or busy masked.
  // disregarding the ctp link out.
  if (m_busy_with_ltplink) {
    status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_LTP);
    ERS_LOG(sout << "Enabling busy for ltp linkout (\"" << m_busy_ltplink_uid << "\")");
  } else {
    status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_DISABLED);
    ERS_LOG(sout << "Disabling busy for ltp linkout (\"" << m_busy_ltplink_uid << "\")");
  }
  

// check that I2C is working: I2C clock frequency 100 KHz, otherwise reset it. 
  m_ltpi->readI2CFrequency(frequency); 
  if (frequency != 100.0) { 
    ERS_LOG(sout << "I2C frequency not set to 100.0 KHz; trying to setup I2C"); 
    // set I2C 
    m_ltpi->init();
    m_ltpi->readI2CFrequency(frequency); 
    if (frequency != 100.0) {
      ostringstream text; 
      text << sout << "Cannot set I2C clock frequency to 100.0 KHz"; 
      ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), ); 
    } else {
      if (m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl) != 0) { 
	ostringstream text; 
	text << sout << "Bus error while accessing DAC with I2C cycle"; 
	ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), ); 
      } 
    }
  }

  // configure DAC
  bool enable = true;
  bool IsShort = false;
  if(a_LTPcable_length <= 20)  IsShort = true;

  if ((status = m_ltpi->writeLinkEqualization(LTPI::EQUAL_LTP,enable,IsShort)) == LTPI::SUCCESS) {
    ERS_LOG(sout << "LTP DAC set for cable length :" << a_LTPcable_length << " m");
    // to report DAC values for information
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "LTP DAC gain set to: " << std::hex << val_gain << std::dec);
    m_LTP_in_gain = val_gain;
    ERS_LOG(sout << "LTP DAC equalizer set to: " << std::hex << val_ctrl << std::dec);
    m_LTP_in_equ = val_ctrl;
  } else {
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "LTP DAC gain current value: " << std::hex << val_gain << std::dec);
    m_LTP_in_gain = val_gain;
    ERS_LOG(sout << "LTP DAC equalizer current value: " << std::hex << val_ctrl << std::dec);
    m_LTP_in_equ = val_ctrl;
    ostringstream text; 
    text << sout << "DAC couldn't be set";
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }

   
  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status = :" << status << " (0x" << hex << status << dec << ")";
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }
  
  ERS_LOG(sout << "Done");
}


void LTPiModule::configureCTPSlave(const LTPidal::LTPi_CTPSlave* c) {
  std::string sout = m_UID + "@LTPiModule::configureCTPSlave() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "called with LTPi_CTPSlave = " << c);
  c->print(1, true, std::cout);

  u_int a_CTPcable_length = c->get_CTPcable_length();
  ERS_LOG(sout << "CTPcable_length = " << a_CTPcable_length);

  u_int a_Delay_L1A = c->get_Delay_L1A();
  m_Delay_L1A = a_Delay_L1A;
  ERS_LOG(sout << "Delay_L1A = " << a_Delay_L1A);
  u_int a_Delay_Orbit = c->get_Delay_Orbit();
  m_Delay_Orbit = a_Delay_Orbit;
  ERS_LOG(sout << "Delay_Orbit = " << a_Delay_Orbit);
  u_int a_Delay_TTR1 = c->get_Delay_TTR1();
  m_Delay_TTR1 = a_Delay_TTR1;
  ERS_LOG(sout << "Delay_TTR1 = " << a_Delay_TTR1);
  u_int a_Delay_TTR2 = c->get_Delay_TTR2();
  m_Delay_TTR2 = a_Delay_TTR2;
  ERS_LOG(sout << "Delay_TTR2 = " << a_Delay_TTR2);
  u_int a_Delay_TTR3 = c->get_Delay_TTR3();
  m_Delay_TTR3 = a_Delay_TTR3;
  ERS_LOG(sout << "Delay_TTR3 = " << a_Delay_TTR3);
  u_int a_Delay_TTYPE0 = c->get_Delay_TTYPE0();
  m_Delay_TTYPE0 = a_Delay_TTYPE0;
  ERS_LOG(sout << "Delay_TTYPE0 = " << a_Delay_TTYPE0);
  u_int a_Delay_TTYPE1 = c->get_Delay_TTYPE1();
  m_Delay_TTYPE1 = a_Delay_TTYPE1;
  ERS_LOG(sout << "Delay_TTYPE1 = " << a_Delay_TTYPE1);
  u_int a_Delay_TTYPE2 = c->get_Delay_TTYPE2();
  m_Delay_TTYPE2 = a_Delay_TTYPE2;
  ERS_LOG(sout << "Delay_TTYPE2 = " << a_Delay_TTYPE2);
  u_int a_Delay_TTYPE3 = c->get_Delay_TTYPE3();
  m_Delay_TTYPE3 = a_Delay_TTYPE3;
  ERS_LOG(sout << "Delay_TTYPE3 = " << a_Delay_TTYPE3);
  u_int a_Delay_TTYPE4 = c->get_Delay_TTYPE4();
  m_Delay_TTYPE4 = a_Delay_TTYPE4;
  ERS_LOG(sout << "Delay_TTYPE4 = " << a_Delay_TTYPE4);
  u_int a_Delay_TTYPE5 = c->get_Delay_TTYPE5();
  m_Delay_TTYPE5 = a_Delay_TTYPE5;
  ERS_LOG(sout << "Delay_TTYPE5 = " << a_Delay_TTYPE5);
  u_int a_Delay_TTYPE6 = c->get_Delay_TTYPE6();
  m_Delay_TTYPE6 = a_Delay_TTYPE6;
  ERS_LOG(sout << "Delay_TTYPE6 = " << a_Delay_TTYPE6);
  u_int a_Delay_TTYPE7 = c->get_Delay_TTYPE7();
  m_Delay_TTYPE7 = a_Delay_TTYPE7;
  ERS_LOG(sout << "Delay_TTYPE7 = " << a_Delay_TTYPE7);
  u_int a_Delay_BGo0 = c->get_Delay_BGo0();
  m_Delay_BGO0 = a_Delay_BGo0;
  ERS_LOG(sout << "Delay_BGo0 = " << a_Delay_BGo0);
  u_int a_Delay_BGo1 = c->get_Delay_BGo1();
  m_Delay_BGO1 = a_Delay_BGo1;
  ERS_LOG(sout << "Delay_BGo1 = " << a_Delay_BGo1);
  u_int a_Delay_BGo2 = c->get_Delay_BGo2();
  m_Delay_BGO2 = a_Delay_BGo2;
  ERS_LOG(sout << "Delay_BGo2 = " << a_Delay_BGo2);
  u_int a_Delay_BGo3 = c->get_Delay_BGo3();
  m_Delay_BGO3 = a_Delay_BGo3;
  ERS_LOG(sout << "Delay_BGo3 = " << a_Delay_BGo3);

  // configure
  int status = 0;
  u_short val_gain, val_ctrl;
  double frequency;
  // configure busy mask according to values obtained from database during setup()
  // touching only the ctp link in. Either busy from ctp link out or busy masked.
  // disregarding the ctp link in.
 if (m_busy_with_ctplink) {
    status |= m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_CTP);
    ERS_LOG(sout << "Enabling busy for ctp linkout (\"" << m_busy_ctplink_uid << "\")");
  } else {
    status |= m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_DISABLED);
    ERS_LOG(sout << "Disabling busy for ctp linkout (\"" << m_busy_ctplink_uid << "\")");
  }

  // check that I2C is working: I2C clock frequency 100 KHz, otherwise reset it. 
  m_ltpi->readI2CFrequency(frequency); 
  if (frequency != 100.0) { 
    ERS_LOG(sout << "I2C frequency not set to 100.0 KHz; trying to setup I2C"); 
    // set I2C 
    m_ltpi->init();
    m_ltpi->readI2CFrequency(frequency); 
    if (frequency != 100.0) {
      ostringstream text; 
      text << sout << "Cannot set I2C clock frequency to 100.0 KHz"; 
      ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), ); 
    } else {
      if (m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl) != 0) { 
	ostringstream text; 
	text << sout << "Bus error while accessing DAC with I2C cycle"; 
	ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), ); 
      } 
    }
  }

 // configure DAC
  bool enable = true;
  bool IsShort = false;
  if(a_CTPcable_length <= 20)  IsShort = true;

  if ((status = m_ltpi->writeLinkEqualization(LTPI::EQUAL_CTP,enable,IsShort)) == LTPI::SUCCESS) {
    ERS_LOG(sout << "LTP DAC set for cable length :" << a_CTPcable_length << " m");
    // to report DAC values for information
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_CTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "CTP DAC gain set to: " << std::hex << val_gain << std::dec);
    m_CTP_in_gain = val_gain;
    ERS_LOG(sout << "CTP DAC equalizer set to: " << std::hex << val_ctrl << std::dec);
    m_CTP_in_equ = val_ctrl;
  } else {
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_CTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "CTP DAC gain current value: " << std::hex << val_gain << std::dec);
    m_CTP_in_gain = val_gain;
    ERS_LOG(sout << "CTP DAC equalizer current value: " << std::hex << val_ctrl << std::dec);
    m_CTP_in_equ = val_ctrl;
    ostringstream text; 
    text << sout << "DAC couldn't be set";
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }


 // configure DELAYs 
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_L1A,a_Delay_L1A);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_ORB,a_Delay_Orbit);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG1,a_Delay_TTR1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG2,a_Delay_TTR2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG3,a_Delay_TTR3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO0,a_Delay_BGo0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO1,a_Delay_BGo1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO2,a_Delay_BGo2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO3,a_Delay_BGo3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP0,a_Delay_TTYPE0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP1,a_Delay_TTYPE1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP2,a_Delay_TTYPE2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP3,a_Delay_TTYPE3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP4,a_Delay_TTYPE4);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP5,a_Delay_TTYPE5);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP6,a_Delay_TTYPE6);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP7,a_Delay_TTYPE7);

  // Switching:
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_CTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_CTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_CTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_CTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_CTP);
  status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_CTP,LTPI::LOCTTYP_FROM_CTP_TTRG);
  status = m_ltpi->writeCalibrationRequestSelection(LTPI::CALREQ_FROM_CTP);
  

  // Done

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

void LTPiModule::configureExpert(const LTPidal::LTPi_Expert* c) {

  std::string sout = m_UID + "@LTPiModule::configureExpert() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "called with LTPi_Expert = " << c);
  c->print(1, true, std::cout);
  u_int a_CTP_in_gain = c->get_CTP_in_gain();
  m_CTP_in_gain = a_CTP_in_gain;
  ERS_LOG(sout << "CTP_in_gain = " << a_CTP_in_gain);
  u_int a_LTP_in_gain = c->get_LTP_in_gain();
  m_LTP_in_gain = a_LTP_in_gain;
  ERS_LOG(sout << "LTP_in_gain = " << a_LTP_in_gain);
  u_int a_CTP_in_equ = c->get_CTP_in_equ();
  m_CTP_in_equ = a_CTP_in_equ;
  ERS_LOG(sout << "CTP_in_equ = " << a_CTP_in_equ);
  u_int a_LTP_in_equ = c->get_LTP_in_equ();
  m_LTP_in_equ = a_LTP_in_equ;
  ERS_LOG(sout << "LTP_in_equ = " << a_LTP_in_equ);
  u_int a_Delay_L1A = c->get_Delay_L1A();
  m_Delay_L1A = a_Delay_L1A;
  ERS_LOG(sout << "Delay_L1A = " << a_Delay_L1A);
  u_int a_Delay_Orbit = c->get_Delay_Orbit();
  m_Delay_Orbit = a_Delay_Orbit;
  ERS_LOG(sout << "Delay_Orbit = " << a_Delay_Orbit);
  u_int a_Delay_TTR1 = c->get_Delay_TTR1();
  m_Delay_TTR1 = a_Delay_TTR1;
  ERS_LOG(sout << "Delay_TTR1 = " << a_Delay_TTR1);
  u_int a_Delay_TTR2 = c->get_Delay_TTR2();
  m_Delay_TTR2 = a_Delay_TTR2;
  ERS_LOG(sout << "Delay_TTR2 = " << a_Delay_TTR2);
  u_int a_Delay_TTR3 = c->get_Delay_TTR3();
  m_Delay_TTR3 = a_Delay_TTR3;
  ERS_LOG(sout << "Delay_TTR3 = " << a_Delay_TTR3);
  u_int a_Delay_TTYPE0 = c->get_Delay_TTYPE0();
  m_Delay_TTYPE0 = a_Delay_TTYPE0;
  ERS_LOG(sout << "Delay_TTYPE0 = " << a_Delay_TTYPE0);
  u_int a_Delay_TTYPE1 = c->get_Delay_TTYPE1();
  m_Delay_TTYPE1 = a_Delay_TTYPE1;
  ERS_LOG(sout << "Delay_TTYPE1 = " << a_Delay_TTYPE1);
  u_int a_Delay_TTYPE2 = c->get_Delay_TTYPE2();
  m_Delay_TTYPE2 = a_Delay_TTYPE2;
  ERS_LOG(sout << "Delay_TTYPE2 = " << a_Delay_TTYPE2);
  u_int a_Delay_TTYPE3 = c->get_Delay_TTYPE3();
  m_Delay_TTYPE3 = a_Delay_TTYPE3;
  ERS_LOG(sout << "Delay_TTYPE3 = " << a_Delay_TTYPE3);
  u_int a_Delay_TTYPE4 = c->get_Delay_TTYPE4();
  m_Delay_TTYPE4 = a_Delay_TTYPE4;
  ERS_LOG(sout << "Delay_TTYPE4 = " << a_Delay_TTYPE4);
  u_int a_Delay_TTYPE5 = c->get_Delay_TTYPE5();
  m_Delay_TTYPE5 = a_Delay_TTYPE5;
  ERS_LOG(sout << "Delay_TTYPE5 = " << a_Delay_TTYPE5);
  u_int a_Delay_TTYPE6 = c->get_Delay_TTYPE6();
  m_Delay_TTYPE6 = a_Delay_TTYPE6;
  ERS_LOG(sout << "Delay_TTYPE6 = " << a_Delay_TTYPE6);
  u_int a_Delay_TTYPE7 = c->get_Delay_TTYPE7();
  m_Delay_TTYPE7 = a_Delay_TTYPE7;
  ERS_LOG(sout << "Delay_TTYPE7 = " << a_Delay_TTYPE7);
  u_int a_Delay_BGo0 = c->get_Delay_BGo0();
  m_Delay_BGO0 = a_Delay_BGo0;
  ERS_LOG(sout << "Delay_BGo0 = " << a_Delay_BGo0);
  u_int a_Delay_BGo1 = c->get_Delay_BGo1();
  m_Delay_BGO1 = a_Delay_BGo1;
  ERS_LOG(sout << "Delay_BGo1 = " << a_Delay_BGo1);
  u_int a_Delay_BGo2 = c->get_Delay_BGo2();
  m_Delay_BGO2 = a_Delay_BGo2;
  ERS_LOG(sout << "Delay_BGo2 = " << a_Delay_BGo2);
  u_int a_Delay_BGo3 = c->get_Delay_BGo3();
  m_Delay_BGO3 = a_Delay_BGo3;
  ERS_LOG(sout << "Delay_BGo3 = " << a_Delay_BGo3);
  string a_BC_routing = c->get_BC_routing();
  ERS_LOG(sout << "BC_routing = " << a_BC_routing);
  string a_ORB_routing = c->get_ORB_routing();
  ERS_LOG(sout << "ORB_routing = " << a_ORB_routing);
  string a_L1A_routing = c->get_L1A_routing();
  ERS_LOG(sout << "L1A_routing = " << a_L1A_routing);
  string a_TTR_routing = c->get_TTR_routing();
  ERS_LOG(sout << "TTR_routing = " << a_TTR_routing);
  string a_BGO_routing = c->get_BGO_routing();
  ERS_LOG(sout << "BGO_routing = " << a_BGO_routing);
  string a_TRT_routing = c->get_TRT_routing();
  ERS_LOG(sout << "TRT_routing = " << a_TRT_routing);
  string a_TTYPE_MODE = c->get_TTYPE_MODE();
  ERS_LOG(sout << "TTYPE_MODE = " << a_TTYPE_MODE);
  u_int a_TTYPE_REGISTER = c->get_TTYPE_REGISTER();
  ERS_LOG(sout << "TTYPE_REGISTER = " << hex << a_TTYPE_REGISTER << dec);
  u_int a_LUT_TTYPE_000 = c->get_LUT_TTYPE_000();
  ERS_LOG(sout << "LUT_TTYPE_000 = " << hex << a_LUT_TTYPE_000 << dec);
  u_int a_LUT_TTYPE_001 = c->get_LUT_TTYPE_001();
  ERS_LOG(sout << "LUT_TTYPE_001 = " << hex << a_LUT_TTYPE_001 << dec);
  u_int a_LUT_TTYPE_010 = c->get_LUT_TTYPE_010();
  ERS_LOG(sout << "LUT_TTYPE_010 = " << hex << a_LUT_TTYPE_010 << dec);
  u_int a_LUT_TTYPE_011 = c->get_LUT_TTYPE_011();
  ERS_LOG(sout << "LUT_TTYPE_011 = " << hex << a_LUT_TTYPE_011 << dec);
  u_int a_LUT_TTYPE_100 = c->get_LUT_TTYPE_100();
  ERS_LOG(sout << "LUT_TTYPE_100 = " << hex << a_LUT_TTYPE_100 << dec);
  u_int a_LUT_TTYPE_101 = c->get_LUT_TTYPE_101();
  ERS_LOG(sout << "LUT_TTYPE_101 = " << hex << a_LUT_TTYPE_101 << dec);
  u_int a_LUT_TTYPE_110 = c->get_LUT_TTYPE_110();
  ERS_LOG(sout << "LUT_TTYPE_110 = " << hex << a_LUT_TTYPE_110 << dec);
  u_int a_LUT_TTYPE_111 = c->get_LUT_TTYPE_111();
  ERS_LOG(sout << "LUT_TTYPE_111 = " << hex << a_LUT_TTYPE_111 << dec);
  string a_Busy_CTP_in = c->get_Busy_CTP_in();
  ERS_LOG(sout << "Busy_CTP_in = " << a_Busy_CTP_in);
  string a_Busy_LTP_in = c->get_Busy_LTP_in();
  ERS_LOG(sout << "Busy_LTP_in = " << a_Busy_LTP_in);
  string a_Busy_NIM = c->get_Busy_NIM();
  ERS_LOG(sout << "Busy_NIM = " << a_Busy_NIM);

  // configure 
  int status = 0;
  // reset the module
  status = m_ltpi->reset();
  // set I2C
  status = m_ltpi->init();

  // configure busy mask according to values obtained from database during setup()
  // configure all three busy outputs CTP, LTP, NIM.


  // CTP Link in busy
  if (a_Busy_CTP_in == LTPidal::LTPi_Expert::Busy_CTP_in::OFF) status |= m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_DISABLED);

  if (a_Busy_CTP_in == LTPidal::LTPi_Expert::Busy_CTP_in::CTP_out)
    // check if the CTP_out signal is active as abtained from the database
    if (m_busy_with_ctplink) status = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_CTP);
    
  if (a_Busy_CTP_in == LTPidal::LTPi_Expert::Busy_CTP_in::LTP_out)
    // check if the LTP_out signal is active as abtained from the database
    if (m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_LTP);

  if (a_Busy_CTP_in == LTPidal::LTPi_Expert::Busy_CTP_in::CTP_out_and_LTP_out) {
    // check if the CTP_out and the LTP_out signals are active as abtained from the database
    if (m_busy_with_ctplink && (!m_busy_with_ltplink))      status = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_CTP);
    else if ((!m_busy_with_ctplink) && m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_LTP);
    else if (m_busy_with_ctplink && m_busy_with_ltplink)    status = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_FROM_CTP_OR_LTP);
  }

  // LTP Link in busy
  if (a_Busy_LTP_in == LTPidal::LTPi_Expert::Busy_LTP_in::OFF) status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_DISABLED);

  if (a_Busy_LTP_in == LTPidal::LTPi_Expert::Busy_LTP_in::CTP_out)
    // check if the CTP_out signal is active as abtained from the database
    if (m_busy_with_ctplink) status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_CTP);
    
  if (a_Busy_LTP_in == LTPidal::LTPi_Expert::Busy_LTP_in::LTP_out)
    // check if the LTP_out signal is active as abtained from the database
    if (m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_LTP);
    
  if (a_Busy_LTP_in == LTPidal::LTPi_Expert::Busy_LTP_in::CTP_out_and_LTP_out) {
    // check if the CTP_out and the LTP_out signals are active as abtained from the database
    if (m_busy_with_ctplink && (!m_busy_with_ltplink))      status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_CTP);
    else if ((!m_busy_with_ctplink) && m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_LTP);
    else if (m_busy_with_ctplink && m_busy_with_ltplink)    status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_CTP_OR_LTP);
  }

  // NIM Link in busy
  if (a_Busy_NIM == LTPidal::LTPi_Expert::Busy_NIM::OFF) status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_DISABLED);

  if (a_Busy_NIM == LTPidal::LTPi_Expert::Busy_NIM::CTP_out)
    // check if the CTP_out signal is active as abtained from the database
    // Ask why busyLTP instead of busyNIM 
    if (m_busy_with_ctplink) status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_CTP);
    
  if (a_Busy_NIM == LTPidal::LTPi_Expert::Busy_NIM::LTP_out)
    // check if the CTP_out signal is active as abtained from the database
    // Ask why busyLTP instead of busyNIM 
    if (m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_LTP);
    
  if (a_Busy_NIM == LTPidal::LTPi_Expert::Busy_NIM::CTP_out_and_LTP_out) {
    // check if the CTP_out and the LTP_out signals are active as abtained from the database
    if (m_busy_with_ctplink && (!m_busy_with_ltplink))      status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_CTP);
    else if ((!m_busy_with_ctplink) && m_busy_with_ltplink) status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_LTP);
    else if (m_busy_with_ctplink && m_busy_with_ltplink)    status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_CTP_OR_LTP);
  }

  // configure DAC
  status = m_ltpi->writeLinkEqualization(LTPI::EQUAL_CTP,(u_short) a_CTP_in_gain,(u_short) a_CTP_in_equ);
  status = m_ltpi->writeLinkEqualization(LTPI::EQUAL_LTP,(u_short) a_LTP_in_gain,(u_short) a_LTP_in_equ);  

  // configure DELAYs
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_L1A,a_Delay_L1A);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_ORB,a_Delay_Orbit);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG1,a_Delay_TTR1);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG2,a_Delay_TTR2);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG3,a_Delay_TTR3);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_BGO0,a_Delay_BGo0);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_BGO1,a_Delay_BGo1);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_BGO2,a_Delay_BGo2);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_BGO3,a_Delay_BGo3);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP0,a_Delay_TTYPE0);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP1,a_Delay_TTYPE1);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP2,a_Delay_TTYPE2);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP3,a_Delay_TTYPE3);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP4,a_Delay_TTYPE4);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP5,a_Delay_TTYPE5);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP6,a_Delay_TTYPE6);
  status = m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP7,a_Delay_TTYPE7);

  // configure trigger type

  status = m_ltpi->writeLocalTriggerTypeWord(0,a_LUT_TTYPE_000);
  status = m_ltpi->writeLocalTriggerTypeWord(1,a_LUT_TTYPE_001);
  status = m_ltpi->writeLocalTriggerTypeWord(2,a_LUT_TTYPE_010);
  status = m_ltpi->writeLocalTriggerTypeWord(3,a_LUT_TTYPE_011);
  status = m_ltpi->writeLocalTriggerTypeWord(4,a_LUT_TTYPE_100);
  status = m_ltpi->writeLocalTriggerTypeWord(5,a_LUT_TTYPE_101);
  status = m_ltpi->writeLocalTriggerTypeWord(6,a_LUT_TTYPE_110);
  status = m_ltpi->writeLocalTriggerTypeWord(7,a_LUT_TTYPE_111);

  // Switching:
  // setting BC 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::OFF)                   status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_DISABLED); 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::CTP_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_PARALLEL); 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::CTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_CTP); 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::LTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_LTP); 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::NIM_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_NIM); 
  if (a_BC_routing == LTPidal::LTPi_Expert::BC_routing::NIM_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_PARALLEL_FROM_NIM); 

  // setting Orbit
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::OFF)                   status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_DISABLED); 
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::CTP_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_PARALLEL); 
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::CTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_CTP); 
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::LTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_LTP); 
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::NIM_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_NIM); 
  if (a_ORB_routing == LTPidal::LTPi_Expert::ORB_routing::NIM_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_PARALLEL_FROM_NIM); 

  // setting L1A
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::OFF)                   status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_DISABLED); 
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::CTP_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_PARALLEL); 
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::CTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_CTP); 
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::LTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_LTP); 
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::NIM_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_NIM); 
  if (a_L1A_routing == LTPidal::LTPi_Expert::L1A_routing::NIM_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_PARALLEL_FROM_NIM); 

  // setting TTR
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::OFF)                   status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_DISABLED); 
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::CTP_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_PARALLEL); 
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::CTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_CTP); 
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::LTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_LTP); 
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::NIM_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_NIM); 
  if (a_TTR_routing == LTPidal::LTPi_Expert::TTR_routing::NIM_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_PARALLEL_FROM_NIM); 

  // setting BGO
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::OFF)                   status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_DISABLED); 
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::CTP_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_PARALLEL); 
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::CTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_CTP); 
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::LTP_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_LTP); 
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::NIM_TO_CTP_LTP)        status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_NIM); 
  if (a_BGO_routing == LTPidal::LTPi_Expert::BGO_routing::NIM_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_PARALLEL_FROM_NIM); 

  // setting TRT
  if (a_TTYPE_MODE == LTPidal::LTPi_Expert::TTYPE_MODE::REGISTER) { 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::OFF)                     status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_DISABLED,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::CTP_TO_CTP_LTP_TO_LTP)   status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_PARALLEL,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::CTP_TO_CTP_LTP)          status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_CTP,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LTP_TO_CTP_LTP)          status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LTP,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LOCAL_TO_CTP_LTP)        status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LOCAL,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LOCAL_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_PARALLEL_FROM_LOCAL,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
  } 
  else if (a_TTYPE_MODE == LTPidal::LTPi_Expert::TTYPE_MODE::LUT) {
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::OFF)                     status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_DISABLED,LTPI::LOCTTYP_FROM_NIM_TTRG); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::CTP_TO_CTP_LTP_TO_LTP)   status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_PARALLEL,LTPI::LOCTTYP_FROM_NIM_TTRG); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::CTP_TO_CTP_LTP)          status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_CTP,LTPI::LOCTTYP_FROM_NIM_TTRG); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LTP_TO_CTP_LTP)          status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LTP,LTPI::LOCTTYP_FROM_NIM_TTRG); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LOCAL_TO_CTP_LTP)        status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LOCAL,LTPI::LOCTTYP_FROM_NIM_TTRG); 
    if (a_TRT_routing == LTPidal::LTPi_Expert::TRT_routing::LOCAL_TO_CTP_LTP_TO_LTP) status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_PARALLEL_FROM_LOCAL,LTPI::LOCTTYP_FROM_NIM_TTRG); 
  } 
  else {
    ostringstream text;
    text << sout << "Wrong trigger type TTYPE_MODE = " << a_TTYPE_MODE;
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }
  
  // Done

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

void LTPiModule::configureLTPMaster(const LTPidal::LTPi_LTPMaster* c) {

  std::string sout = m_UID + "@LTPiModule::configureLTPMaster() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "called with LTPi_LTPMaster = " << c);
  c->print(1, true, std::cout);

  u_int a_Delay_L1A = c->get_Delay_L1A();
  m_Delay_L1A = a_Delay_L1A;
  ERS_LOG(sout << "Delay_L1A = " << a_Delay_L1A);
  u_int a_Delay_Orbit = c->get_Delay_Orbit();
  m_Delay_Orbit = a_Delay_Orbit;
  ERS_LOG(sout << "Delay_Orbit = " << a_Delay_Orbit);
  u_int a_Delay_TTR1 = c->get_Delay_TTR1();
  m_Delay_TTR1 = a_Delay_TTR1;
  ERS_LOG(sout << "Delay_TTR1 = " << a_Delay_TTR1);
  u_int a_Delay_TTR2 = c->get_Delay_TTR2();
  m_Delay_TTR2 = a_Delay_TTR2;
  ERS_LOG(sout << "Delay_TTR2 = " << a_Delay_TTR2);
  u_int a_Delay_TTR3 = c->get_Delay_TTR3();
  m_Delay_TTR3 = a_Delay_TTR3;
  ERS_LOG(sout << "Delay_TTR3 = " << a_Delay_TTR3);
  u_int a_Delay_TTYPE0 = c->get_Delay_TTYPE0();
  m_Delay_TTYPE0 = a_Delay_TTYPE0;
  ERS_LOG(sout << "Delay_TTYPE0 = " << a_Delay_TTYPE0);
  u_int a_Delay_TTYPE1 = c->get_Delay_TTYPE1();
  m_Delay_TTYPE1 = a_Delay_TTYPE1;
  ERS_LOG(sout << "Delay_TTYPE1 = " << a_Delay_TTYPE1);
  u_int a_Delay_TTYPE2 = c->get_Delay_TTYPE2();
  m_Delay_TTYPE2 = a_Delay_TTYPE2;
  ERS_LOG(sout << "Delay_TTYPE2 = " << a_Delay_TTYPE2);
  u_int a_Delay_TTYPE3 = c->get_Delay_TTYPE3();
  m_Delay_TTYPE3 = a_Delay_TTYPE3;
  ERS_LOG(sout << "Delay_TTYPE3 = " << a_Delay_TTYPE3);
  u_int a_Delay_TTYPE4 = c->get_Delay_TTYPE4();
  m_Delay_TTYPE4 = a_Delay_TTYPE4;
  ERS_LOG(sout << "Delay_TTYPE4 = " << a_Delay_TTYPE4);
  u_int a_Delay_TTYPE5 = c->get_Delay_TTYPE5();
  m_Delay_TTYPE5 = a_Delay_TTYPE5;
  ERS_LOG(sout << "Delay_TTYPE5 = " << a_Delay_TTYPE5);
  u_int a_Delay_TTYPE6 = c->get_Delay_TTYPE6();
  m_Delay_TTYPE6 = a_Delay_TTYPE6;
  ERS_LOG(sout << "Delay_TTYPE6 = " << a_Delay_TTYPE6);
  u_int a_Delay_TTYPE7 = c->get_Delay_TTYPE7();
  m_Delay_TTYPE7 = a_Delay_TTYPE7;
  ERS_LOG(sout << "Delay_TTYPE7 = " << a_Delay_TTYPE7);
  u_int a_Delay_BGo0 = c->get_Delay_BGo0();
  m_Delay_BGO0 = a_Delay_BGo0;
  ERS_LOG(sout << "Delay_BGo0 = " << a_Delay_BGo0);
  u_int a_Delay_BGo1 = c->get_Delay_BGo1();
  m_Delay_BGO1 = a_Delay_BGo1;
  ERS_LOG(sout << "Delay_BGo1 = " << a_Delay_BGo1);
  u_int a_Delay_BGo2 = c->get_Delay_BGo2();
  m_Delay_BGO2 = a_Delay_BGo2;
  ERS_LOG(sout << "Delay_BGo2 = " << a_Delay_BGo2);
  u_int a_Delay_BGo3 = c->get_Delay_BGo3();
  m_Delay_BGO3 = a_Delay_BGo3;
  ERS_LOG(sout << "Delay_BGo3 = " << a_Delay_BGo3);
  string a_TTYPE_MODE = c->get_TTYPE_MODE();
  ERS_LOG(sout << "TTYPE_MODE = " << a_TTYPE_MODE);
  u_int a_TTYPE_REGISTER = c->get_TTYPE_REGISTER();
  ERS_LOG(sout << "TTYPE_REGISTER = " << hex << a_TTYPE_REGISTER << dec);
  u_int a_LUT_TTYPE_000 = c->get_LUT_TTYPE_000();
  ERS_LOG(sout << "LUT_TTYPE_000 = " << hex << a_LUT_TTYPE_000 << dec);
  u_int a_LUT_TTYPE_001 = c->get_LUT_TTYPE_001();
  ERS_LOG(sout << "LUT_TTYPE_001 = " << hex << a_LUT_TTYPE_001 << dec);
  u_int a_LUT_TTYPE_010 = c->get_LUT_TTYPE_010();
  ERS_LOG(sout << "LUT_TTYPE_010 = " << hex << a_LUT_TTYPE_010 << dec);
  u_int a_LUT_TTYPE_011 = c->get_LUT_TTYPE_011();
  ERS_LOG(sout << "LUT_TTYPE_011 = " << hex << a_LUT_TTYPE_011 << dec);
  u_int a_LUT_TTYPE_100 = c->get_LUT_TTYPE_100();
  ERS_LOG(sout << "LUT_TTYPE_100 = " << hex << a_LUT_TTYPE_100 << dec);
  u_int a_LUT_TTYPE_101 = c->get_LUT_TTYPE_101();
  ERS_LOG(sout << "LUT_TTYPE_101 = " << hex << a_LUT_TTYPE_101 << dec);
  u_int a_LUT_TTYPE_110 = c->get_LUT_TTYPE_110();
  ERS_LOG(sout << "LUT_TTYPE_110 = " << hex << a_LUT_TTYPE_110 << dec);
  u_int a_LUT_TTYPE_111 = c->get_LUT_TTYPE_111();
  ERS_LOG(sout << "LUT_TTYPE_111 = " << hex << a_LUT_TTYPE_111 << dec);

  
 // configure 
  int status = 0;
  // reset the module
  status = m_ltpi->reset();
  // set I2C
  status = m_ltpi->init();
 // configure busy mask according to values obtained from database during setup()
  // touching only the NIM. Either busy from ltp link in or busy masked.
  // disregarding the ctp link in.
  if (m_busy_with_ltplink) {
    status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_FROM_LTP);
    ERS_LOG(sout << "Enabling busy for ltp linkout (\"" << m_busy_ltplink_uid << "\")");
  } else {
    status = m_ltpi->writeBusySelection(LTPI::NIM,LTPI::BUSY_DISABLED);
    ERS_LOG(sout << "Disabling busy for ltp linkout (\"" << m_busy_ltplink_uid << "\")");
  }

 // configure DELAYs
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_L1A,a_Delay_L1A);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_ORB,a_Delay_Orbit);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG1,a_Delay_TTR1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG2,a_Delay_TTR2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG3,a_Delay_TTR3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO0,a_Delay_BGo0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO1,a_Delay_BGo1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO2,a_Delay_BGo2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO3,a_Delay_BGo3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP0,a_Delay_TTYPE0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP1,a_Delay_TTYPE1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP2,a_Delay_TTYPE2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP3,a_Delay_TTYPE3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP4,a_Delay_TTYPE4);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP5,a_Delay_TTYPE5);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP6,a_Delay_TTYPE6);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP7,a_Delay_TTYPE7);

  // configure trigger type
  status = m_ltpi->writeLocalTriggerTypeWord(0,a_LUT_TTYPE_000);
  status = m_ltpi->writeLocalTriggerTypeWord(1,a_LUT_TTYPE_001);
  status = m_ltpi->writeLocalTriggerTypeWord(2,a_LUT_TTYPE_010);
  status = m_ltpi->writeLocalTriggerTypeWord(3,a_LUT_TTYPE_011);
  status = m_ltpi->writeLocalTriggerTypeWord(4,a_LUT_TTYPE_100);
  status = m_ltpi->writeLocalTriggerTypeWord(5,a_LUT_TTYPE_101);
  status = m_ltpi->writeLocalTriggerTypeWord(6,a_LUT_TTYPE_110);
  status = m_ltpi->writeLocalTriggerTypeWord(7,a_LUT_TTYPE_111);

  if (a_TTYPE_MODE == LTPidal::LTPi_Expert::TTYPE_MODE::REGISTER) {
    status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LOCAL,LTPI::LOCTTYP_FROM_TTYP_WORD0); 
  } else if (a_TTYPE_MODE == LTPidal::LTPi_Expert::TTYPE_MODE::LUT) {
    status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LOCAL,LTPI::LOCTTYP_FROM_NIM_TTRG);
  } else {
      ostringstream text;
      text << sout << "Wrong trigger type TTYPE_MODE = " << a_TTYPE_MODE;
      ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }

  // Switching:
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_NIM);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_NIM);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_NIM);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_NIM);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_NIM);
  
  // Done

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

void LTPiModule::configureLTPSlave(const LTPidal::LTPi_LTPSlave* c) {

  std::string sout = m_UID + "@LTPiModule::configureLTPSlave() ";
  ERS_LOG(sout << "Entered");
  ERS_LOG(sout << "called with LTPi_LTPSlave = " << c);
  c->print(1, true, std::cout);
  u_int a_LTPcable_length = c->get_LTPcable_length();
  ERS_LOG(sout << "LTPcable_length = " << a_LTPcable_length);
  u_int a_Delay_L1A = c->get_Delay_L1A();
  m_Delay_L1A = a_Delay_L1A;
  ERS_LOG(sout << "Delay_L1A = " << a_Delay_L1A);
  u_int a_Delay_Orbit = c->get_Delay_Orbit();
  m_Delay_Orbit = a_Delay_Orbit;
  ERS_LOG(sout << "Delay_Orbit = " << a_Delay_Orbit);
  u_int a_Delay_TTR1 = c->get_Delay_TTR1();
  m_Delay_TTR1 = a_Delay_TTR1;
  ERS_LOG(sout << "Delay_TTR1 = " << a_Delay_TTR1);
  u_int a_Delay_TTR2 = c->get_Delay_TTR2();
  m_Delay_TTR2 = a_Delay_TTR2;
  ERS_LOG(sout << "Delay_TTR2 = " << a_Delay_TTR2);
  u_int a_Delay_TTR3 = c->get_Delay_TTR3();
  m_Delay_TTR3 = a_Delay_TTR3;
  ERS_LOG(sout << "Delay_TTR3 = " << a_Delay_TTR3);
  u_int a_Delay_TTYPE0 = c->get_Delay_TTYPE0();
  m_Delay_TTYPE0 = a_Delay_TTYPE0;
  ERS_LOG(sout << "Delay_TTYPE0 = " << a_Delay_TTYPE0);
  u_int a_Delay_TTYPE1 = c->get_Delay_TTYPE1();
  m_Delay_TTYPE1 = a_Delay_TTYPE1;
  ERS_LOG(sout << "Delay_TTYPE1 = " << a_Delay_TTYPE1);
  u_int a_Delay_TTYPE2 = c->get_Delay_TTYPE2();
  m_Delay_TTYPE2 = a_Delay_TTYPE2;
  ERS_LOG(sout << "Delay_TTYPE2 = " << a_Delay_TTYPE2);
  u_int a_Delay_TTYPE3 = c->get_Delay_TTYPE3();
  m_Delay_TTYPE3 = a_Delay_TTYPE3;
  ERS_LOG(sout << "Delay_TTYPE3 = " << a_Delay_TTYPE3);
  u_int a_Delay_TTYPE4 = c->get_Delay_TTYPE4();
  m_Delay_TTYPE4 = a_Delay_TTYPE4;
  ERS_LOG(sout << "Delay_TTYPE4 = " << a_Delay_TTYPE4);
  u_int a_Delay_TTYPE5 = c->get_Delay_TTYPE5();
  m_Delay_TTYPE5 = a_Delay_TTYPE5;
  ERS_LOG(sout << "Delay_TTYPE5 = " << a_Delay_TTYPE5);
  u_int a_Delay_TTYPE6 = c->get_Delay_TTYPE6();
  m_Delay_TTYPE6 = a_Delay_TTYPE6;
  ERS_LOG(sout << "Delay_TTYPE6 = " << a_Delay_TTYPE6);
  u_int a_Delay_TTYPE7 = c->get_Delay_TTYPE7();
  m_Delay_TTYPE7 = a_Delay_TTYPE7;
  ERS_LOG(sout << "Delay_TTYPE7 = " << a_Delay_TTYPE7);
  u_int a_Delay_BGo0 = c->get_Delay_BGo0();
  m_Delay_BGO0 = a_Delay_BGo0;
  ERS_LOG(sout << "Delay_BGo0 = " << a_Delay_BGo0);
  u_int a_Delay_BGo1 = c->get_Delay_BGo1();
  m_Delay_BGO1 = a_Delay_BGo1;
  ERS_LOG(sout << "Delay_BGo1 = " << a_Delay_BGo1);
  u_int a_Delay_BGo2 = c->get_Delay_BGo2();
  m_Delay_BGO2 = a_Delay_BGo2;
  ERS_LOG(sout << "Delay_BGo2 = " << a_Delay_BGo2);
  u_int a_Delay_BGo3 = c->get_Delay_BGo3();
  m_Delay_BGO3 = a_Delay_BGo3;
  ERS_LOG(sout << "Delay_BGo3 = " << a_Delay_BGo3);

 // configure 
  int status = 0;
  // reset the module
  status = m_ltpi->reset();
  // set I2C
  status = m_ltpi->init();
  // configure DAC
  // touching only the ltp link in. 
  // Busy can come from the ctp out (local ltp connected to the ctp-link-out) or
  // can come from the loop of ltpi connected to the ltp-link-out.
  // check that the busy from the local ltp is active
    // check if the CTP_out and the LTP_out signals are active as abtained from the database
  if (m_busy_with_ctplink && (!m_busy_with_ltplink)) {
    status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_CTP);
    ERS_LOG(sout << "Enabling busy for ctp linkin (\"" << m_busy_ctplink_uid << "\")");
  } else if ((!m_busy_with_ctplink) && m_busy_with_ltplink) {
     status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_LTP);
    ERS_LOG(sout << "Enabling busy for ltp linkin (\"" << m_busy_ltplink_uid << "\")");
  } else if (m_busy_with_ctplink && m_busy_with_ltplink) {
     status = m_ltpi->writeBusySelection(LTPI::LTP,LTPI::BUSY_FROM_CTP_OR_LTP);
    ERS_LOG(sout << "Enabling busy for ctp linkin (\"" << m_busy_ctplink_uid << "\") and for ltp linkin (\"" << m_busy_ltplink_uid << "\")");
  } else if ((!m_busy_with_ctplink) && (!m_busy_with_ltplink)) {
    status  = m_ltpi->writeBusySelection(LTPI::CTP,LTPI::BUSY_DISABLED);
    ERS_LOG(sout << "Disabling busy for ctp linkin (\"" << m_busy_ctplink_uid << "\")  and for ltp linkin (\"" << m_busy_ltplink_uid << "\")");
  }


 double frequency;
  m_ltpi->readI2CFrequency(frequency); 
  if (frequency != 100.0) { 
    ERS_LOG(sout << "I2C frequency not set to 100.0 KHz; resetting module & setting up I2C"); 
    // reset the module 
    status = m_ltpi->reset();
    // set I2C
    status = m_ltpi->init();
  }


  // configure DAC
  bool enable = true;
  bool IsShort = false;
  u_short val_gain(0), val_ctrl(0); 
  
  if(a_LTPcable_length <= 20)  IsShort = true;

  if ((status = m_ltpi->writeLinkEqualization(LTPI::EQUAL_LTP,enable,IsShort)) == LTPI::SUCCESS) {
    ERS_LOG(sout << "LTP DAC set for cable length :" << a_LTPcable_length << " m");
    // to report DAC values for information
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "LTP DAC gain set to: " << std::hex << val_gain << std::dec);
    m_LTP_in_gain = val_gain;
    ERS_LOG(sout << "LTP DAC equalizer set to: " << std::hex << val_ctrl << std::dec);
    m_LTP_in_equ = val_ctrl;
  } else {
    status = m_ltpi->readLinkEqualization(LTPI::EQUAL_LTP,val_gain,val_ctrl); 
    ERS_LOG(sout << "LTP DAC gain current value: " << std::hex << val_gain << std::dec);
    m_LTP_in_gain = val_gain;
    ERS_LOG(sout << "LTP DAC equalizer current value: " << std::hex << val_ctrl << std::dec);
    m_LTP_in_equ = val_ctrl;
    ostringstream text; 
    text << sout << "DAC couldn't be set";
    ERS_REPORT_IMPL( ers::fatal, ers::Message, text.str(), );
  }

// configure DELAYs 
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_L1A,a_Delay_L1A);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_ORB,a_Delay_Orbit);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG1,a_Delay_TTR1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG2,a_Delay_TTR2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTRG3,a_Delay_TTR3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO0,a_Delay_BGo0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO1,a_Delay_BGo1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO2,a_Delay_BGo2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_BGO3,a_Delay_BGo3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP0,a_Delay_TTYPE0);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP1,a_Delay_TTYPE1);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP2,a_Delay_TTYPE2);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP3,a_Delay_TTYPE3);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP4,a_Delay_TTYPE4);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP5,a_Delay_TTYPE5);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP6,a_Delay_TTYPE6);
  status  |= m_ltpi->writeSignalDelay(LTPI::DELAY_TTYP7,a_Delay_TTYPE7);

  // Switching
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BC,LTPI::SIGNAL_FROM_LTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_ORB,LTPI::SIGNAL_FROM_LTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_L1A,LTPI::SIGNAL_FROM_LTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_TTRG,LTPI::SIGNAL_FROM_LTP);
  status = m_ltpi->writeSignalSelection(LTPI::SELECT_BGO,LTPI::SIGNAL_FROM_LTP);
  status = m_ltpi->writeTriggerTypeSelection(LTPI::TTYP_FROM_LTP,LTPI::LOCTTYP_FROM_TTYP_WORD0);
  status = m_ltpi->writeCalibrationRequestSelection(LTPI::CALREQ_FROM_LTP);
  
   // Done

  if (status != 0) {
    ostringstream text;
    text << sout << "VME error, status="<< status << "(0x" << std::hex << status << std::dec << ")";
    ERS_REPORT_IMPL( ers::error, ers::Message, text.str(), );
  }

  ERS_LOG(sout << "Done");
}

extern "C" 
{
  extern ReadoutModule* createLTPiModule ();
}

ReadoutModule *createLTPiModule ()
{
  return (new LTPiModule ());
}

//  LocalWords:  LTP
